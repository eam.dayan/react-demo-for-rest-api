import "./App.css";
import { useEffect, useState } from "react";
import axios from "axios";

function App() {
  useEffect(() => {
    async function makeGetRequest() {
      let res = await axios.get("http://localhost:8080/api/v1/drinks?page=1&size=5");
      let data = res.data;
      return data;
    }

    makeGetRequest().then((res) => {
      console.log(res);
    });
  }, []);

  return <div className='App'></div>;
}

export default App;
